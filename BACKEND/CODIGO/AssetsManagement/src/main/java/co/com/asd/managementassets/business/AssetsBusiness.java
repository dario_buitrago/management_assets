package co.com.asd.managementassets.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.asd.managementassets.Exception.AsdException;
import co.com.asd.managementassets.entities.Assets;
import co.com.asd.managementassets.entities.AssetsState;
import co.com.asd.managementassets.entities.AssetsType;
import co.com.asd.managementassets.entities.Employee;
import co.com.asd.managementassets.repositories.AssetStateRepository;
import co.com.asd.managementassets.repositories.AssetTypeRepository;
import co.com.asd.managementassets.repositories.AssetsRepository;
import co.com.asd.managementassets.repositories.EmployeeRepository;
import co.com.asd.managementassets.request.EditAssetRequest;
import co.com.asd.managementassets.request.FilterSearchRequest;
import co.com.asd.managementassets.utils.ConstantsUtil.StatusCode;

/**
 * Clase que contiene la lógica de negocio conserniente a la adminstración de
 * Activos
 * 
 * @author dario.buitrago
 *
 */
@Service
public class AssetsBusiness {

	/**
	 * variable que inyecta las funcionalidades del repositorio de activos
	 */
	@Autowired
	private AssetsRepository assetsRepository;

	/**
	 * variable que inyecta las funcionalidades del repositorio de empleados
	 */
	@Autowired
	private EmployeeRepository employeeRepository;

	/**
	 * variable que inyecta las funcionalidades del repositorio de tipos de activos
	 */
	@Autowired
	private AssetTypeRepository assetTypeRepository;

	/**
	 * variable que inyecta las funcionalidades del repositorio de estados de
	 * activos
	 */
	@Autowired
	private AssetStateRepository assetStateRepository;

	/**
	 * Método que invoca la consulta a la base de datos para ver todos los activos
	 * 
	 * @return - Lista de activos existentes en la base de datos
	 * @throws AsdException - excepcion de negocio
	 */
	public List<Assets> getAllAssets() throws AsdException {
		List<Assets> listResult = new ArrayList<>();
		try {
			assetsRepository.findAll().forEach(listResult::add);
			if (listResult.isEmpty()) {
				throw new AsdException(StatusCode.NO_RESULT_STATUS, "No existen activos aún");
			}
		} catch (AsdException e) {
			throw e;
		} catch (Exception e) {
			throw new AsdException(StatusCode.INTERNAL_ERROR_STATUS,
					"Se presentó un error mientras se consultaban los activos");
		}
		return listResult;
	}

	/**
	 * Método que invoca la persistencia de un activo nuevo
	 * 
	 * @param asset - objeto de un activo con los parametros a persistir
	 * @return - el objeto Assets con todas sus propiedades
	 * @throws AsdException - Excepcion de negocio
	 */
	public Assets insertAsset(Assets asset) throws AsdException {
		Assets newAsset = new Assets();
		try {
			newAsset = setDataAsset(null, asset);
			newAsset = assetsRepository.save(newAsset);
		} catch (AsdException e) {
			throw e;
		} catch (Exception e) {
			throw new AsdException(StatusCode.INTERNAL_ERROR_STATUS,
					"Se presentó un error mientras se guardaba el activo");
		}
		return newAsset;
	}

	/**
	 * Método que invoca la persistencia de un activo ya existente con el fin de
	 * hacerle cambios
	 * 
	 * @param editRequest - objeto request que contiene el id del activo que se
	 *                    quiere modifica y un objeto Assets con los nuevos valores
	 * @return - Activo modificado con su estado final
	 * @throws AsdException - Excepcion de negocio
	 */
	public Assets editAsset(EditAssetRequest editRequest) throws AsdException {
		Assets newAsset = new Assets();
		try {
			newAsset = setDataAsset(assetsRepository.findByAssetId(editRequest.getAssetIdToEdit()),
					editRequest.getNewAsset());
			newAsset = assetsRepository.save(newAsset);
		} catch (AsdException e) {
			throw e;
		} catch (Exception e) {
			throw new AsdException(StatusCode.INTERNAL_ERROR_STATUS,
					"Se presentó un error mientras se modificaba el activo");
		}
		return newAsset;
	}

	/**
	 * Método que realiza busqueda de Activos con cualquier combinacion de los
	 * siguientes filtros (Serial, tipo de activo y fecha de compra)
	 * 
	 * @param filterSearchRequest - objeto request que contiene los posibles filtros
	 * @return - Lista de Activos que responden a la consulta
	 * @throws AsdException - Excepción de negocio
	 */
	public List<Assets> findByFilter(FilterSearchRequest filterSearchRequest) throws AsdException {
		List<Assets> assetList = new ArrayList<Assets>();
		AssetsType type = null;
		try {
			if (filterSearchRequest == null) {
				throw new AsdException(StatusCode.BAD_REQUEST_STATUS,
						"No se pueden completar la transaccion con los filtros nulo");
			}

			if (filterSearchRequest.getTypeId() != null) {
				type = assetTypeRepository.findByTypeId(filterSearchRequest.getTypeId());
			}

			if ((filterSearchRequest.getStartDate() == null && filterSearchRequest.getEndDate() != null)
					|| (filterSearchRequest.getStartDate() != null && filterSearchRequest.getEndDate() == null)) {
				throw new AsdException(StatusCode.BAD_REQUEST_STATUS,
						"No se puede extraer el resultado con una sola fecha");
			}

			if (filterSearchRequest.getSerial() == null && filterSearchRequest.getTypeId() == null
					&& filterSearchRequest.getStartDate() == null) {
				throw new AsdException(StatusCode.BAD_REQUEST_STATUS, "Se deben agregar parametros a la consulta");
			}

			if (filterSearchRequest.getSerial() != null && filterSearchRequest.getTypeId() != null
					&& filterSearchRequest.getStartDate() != null) {
				assetList = assetsRepository.findByPurchaseDateGreaterThanAndPurchaseDateLessThanEqualAndSerialAndType(
						filterSearchRequest.getStartDate(), filterSearchRequest.getEndDate(),
						filterSearchRequest.getSerial(), type);
			}

			if (filterSearchRequest.getSerial() == null && filterSearchRequest.getTypeId() == null
					&& filterSearchRequest.getStartDate() != null) {
				assetList = assetsRepository.findByPurchaseDateGreaterThanAndPurchaseDateLessThanEqual(
						filterSearchRequest.getStartDate(), filterSearchRequest.getEndDate());
			}

			if (filterSearchRequest.getSerial() != null && filterSearchRequest.getTypeId() == null
					&& filterSearchRequest.getStartDate() == null) {
				assetList = assetsRepository.findBySerial(filterSearchRequest.getSerial());
			}

			if (filterSearchRequest.getSerial() == null && filterSearchRequest.getTypeId() != null
					&& filterSearchRequest.getStartDate() == null) {
				assetList = assetsRepository.findByType(type);
			}

			if (filterSearchRequest.getSerial() != null && filterSearchRequest.getTypeId() != null
					&& filterSearchRequest.getStartDate() == null) {
				assetList = assetsRepository.findBySerialAndType(filterSearchRequest.getSerial(), type);
			}

			if (filterSearchRequest.getSerial() != null && filterSearchRequest.getTypeId() == null
					&& filterSearchRequest.getStartDate() != null) {
				assetList = assetsRepository.findByPurchaseDateGreaterThanAndPurchaseDateLessThanEqualAndSerial(
						filterSearchRequest.getStartDate(), filterSearchRequest.getEndDate(),
						filterSearchRequest.getSerial());
			}

			if (filterSearchRequest.getSerial() == null && filterSearchRequest.getTypeId() != null
					&& filterSearchRequest.getStartDate() != null) {
				assetList = assetsRepository.findByPurchaseDateGreaterThanAndPurchaseDateLessThanEqualAndAndType(
						filterSearchRequest.getStartDate(), filterSearchRequest.getEndDate(), type);
			}

			if (assetList.isEmpty()) {
				throw new AsdException(StatusCode.NO_RESULT_STATUS, "La consulta no generó resultados");
			}

		} catch (AsdException e) {
			throw e;
		} catch (Exception e) {
			throw new AsdException(StatusCode.INTERNAL_ERROR_STATUS,
					"Se presentó un error mientras se modificaba el activo");
		}
		return assetList;
	}

	/**
	 * Metodo que asigna los datos a un activo que se va a persistir
	 * 
	 * @param assetFound - activo encontrado en la base de datos para el caso de la
	 *                   modificacion
	 * @param newAsset   - activo con la nueva informacion que se desea persistir
	 * @return activo con todas sus caracteristicas completas y listas para
	 *         persistir
	 * @throws AsdException - Excepcion de negocio
	 */
	public Assets setDataAsset(Assets assetFound, Assets newAsset) throws AsdException {
		if (newAsset == null) {
			throw new AsdException(StatusCode.BAD_REQUEST_STATUS,
					"No se pueden completar la transaccion con el activo nulo");
		}

		if (newAsset.getEmployeeAsigned() != null && newAsset.getEmployeeAsigned().getEmployeeId() != null) {
			Employee employeeAsigned = employeeRepository
					.findByEmployeeId(newAsset.getEmployeeAsigned().getEmployeeId());
			if (employeeAsigned != null) {
				newAsset.setEmployeeAsigned(employeeAsigned);
			} else {
				throw new AsdException(StatusCode.BAD_REQUEST_STATUS, "El empleado parametrizado no existe");
			}
		} else {
			throw new AsdException(StatusCode.BAD_REQUEST_STATUS,
					"Error estableciendo el empleado que tiene asignado el activo");
		}

		if (newAsset.getType() != null && newAsset.getType().getTypeId() != null) {
			AssetsType type = assetTypeRepository.findByTypeId(newAsset.getType().getTypeId());
			if (type != null) {
				newAsset.setType(type);
			} else {
				throw new AsdException(StatusCode.BAD_REQUEST_STATUS, "El typo de activo parametrizado no existe");
			}
		} else {
			throw new AsdException(StatusCode.BAD_REQUEST_STATUS, "Error estableciendo el tipo de activo");
		}

		if (newAsset.getCurrentState() != null && newAsset.getCurrentState().getStateId() != null) {
			AssetsState assetsState = assetStateRepository.findByStateId(newAsset.getCurrentState().getStateId());
			if (assetsState != null) {
				newAsset.setCurrentState(assetsState);
			} else {
				throw new AsdException(StatusCode.BAD_REQUEST_STATUS, "El estado parametrizado no existe");
			}
		} else {
			throw new AsdException(StatusCode.BAD_REQUEST_STATUS, "Error estableciendo el tipo de activo");
		}

		if (newAsset.getPurchaseDate().after(newAsset.getDowngradeDate()))
			throw new AsdException(StatusCode.BAD_REQUEST_STATUS,
					"La fecha de compra no puede ser superior a la fecha de baja");

		if (assetFound == null) {
			Assets asset = new Assets();
			asset.setEmployeeAsigned(newAsset.getEmployeeAsigned());
			asset.setType(newAsset.getType());
			asset.setCurrentState(newAsset.getCurrentState());
			asset.setAssetLength(newAsset.getAssetLength());
			asset.setColor(newAsset.getColor());
			asset.setDescription(newAsset.getDescription());
			asset.setDowngradeDate(newAsset.getDowngradeDate());
			asset.setHight(newAsset.getHight());
			asset.setInventoryNumber(newAsset.getInventoryNumber());
			asset.setName(newAsset.getName());
			asset.setPurchaseDate(newAsset.getPurchaseDate());
			asset.setPurchasePrice(newAsset.getPurchasePrice());
			asset.setSerial(newAsset.getSerial());
			asset.setWeight(newAsset.getWeight());
			asset.setWidth(newAsset.getWidth());
			return asset;
		} else {
			assetFound.setEmployeeAsigned(newAsset.getEmployeeAsigned());
			assetFound.setType(newAsset.getType());
			assetFound.setCurrentState(newAsset.getCurrentState());
			assetFound.setAssetLength(newAsset.getAssetLength());
			assetFound.setColor(newAsset.getColor());
			assetFound.setDescription(newAsset.getDescription());
			assetFound.setDowngradeDate(newAsset.getDowngradeDate());
			assetFound.setHight(newAsset.getHight());
			assetFound.setInventoryNumber(newAsset.getInventoryNumber());
			assetFound.setName(newAsset.getName());
			assetFound.setPurchaseDate(newAsset.getPurchaseDate());
			assetFound.setPurchasePrice(newAsset.getPurchasePrice());
			assetFound.setSerial(newAsset.getSerial());
			assetFound.setWeight(newAsset.getWeight());
			assetFound.setWidth(newAsset.getWidth());
			return assetFound;
		}
	}
}
