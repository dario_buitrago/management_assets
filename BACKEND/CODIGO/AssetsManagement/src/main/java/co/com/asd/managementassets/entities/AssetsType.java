/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.asd.managementassets.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entidad que representa un tipo de activo
 * 
 * @author dario.buitrago
 */
@Entity
@Table(name = "assets_type")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "AssetsType.findAll", query = "SELECT a FROM AssetsType a"),
		@NamedQuery(name = "AssetsType.findByTypeId", query = "SELECT a FROM AssetsType a WHERE a.typeId = :typeId"),
		@NamedQuery(name = "AssetsType.findByName", query = "SELECT a FROM AssetsType a WHERE a.name = :name"),
		@NamedQuery(name = "AssetsType.findByDescrcription", query = "SELECT a FROM AssetsType a WHERE a.descrcription = :descrcription") })
public class AssetsType implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "TYPE_ID")
	private Integer typeId;

	@Size(max = 45)
	@Column(name = "NAME")
	private String name;

	@Size(max = 100)
	@Column(name = "DESCRCRIPTION")
	private String descrcription;

	@JsonIgnore
	@OneToMany(mappedBy = "type")
	private Collection<Assets> assetsCollection;

	public AssetsType() {
	}

	public AssetsType(Integer typeId) {
		this.typeId = typeId;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescrcription() {
		return descrcription;
	}

	public void setDescrcription(String descrcription) {
		this.descrcription = descrcription;
	}

	@XmlTransient
	public Collection<Assets> getAssetsCollection() {
		return assetsCollection;
	}

	public void setAssetsCollection(Collection<Assets> assetsCollection) {
		this.assetsCollection = assetsCollection;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (typeId != null ? typeId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof AssetsType)) {
			return false;
		}
		AssetsType other = (AssetsType) object;
		if ((this.typeId == null && other.typeId != null)
				|| (this.typeId != null && !this.typeId.equals(other.typeId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.asd.managementassets.entities.AssetsType[ typeId=" + typeId + " ]";
	}

}
