package co.com.asd.managementassets.response;

import java.io.Serializable;

import co.com.asd.managementassets.utils.CodigoRespuestaEnum;

/**
 * Response general del API
 * 
 * @author dario.buitrago
 *
 * @param <T>
 */
public class GeneralResponse<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * mensaje que acompaña la salida del sistema
	 */
	private String message;

	/**
	 * codigo de error estandar
	 */
	private int code;

	/**
	 * Objeto que acompaña la respuesta del sistema
	 */
	private T response;

	public GeneralResponse() {
	}

	/**
	 * Construstor que crea una instancia a partir del codigo estandar y el objeto
	 * 
	 * @param code     - codigo de respuesta estadar
	 * @param response - objeto
	 */
	public GeneralResponse(int code, T response) {
		CodigoRespuestaEnum respuesta = CodigoRespuestaEnum.findByCode(code);
		this.response = response;
		this.code = code;
		this.message = respuesta.getMessage();
	}

	/**
	 * Constructor que crea una instancia del response a partir del codigo estandar
	 * y del mensaje
	 * 
	 * @param code    - codigo estandar
	 * @param message - mensaje de la respuesta
	 */
	public GeneralResponse(int code, String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * Constructor que crea una instancia del response a partir del codigo de
	 * respuesta estandar
	 * 
	 * @param code - codigo de respuesta estandar
	 */
	public GeneralResponse(int code) {
		CodigoRespuestaEnum respuesta = CodigoRespuestaEnum.findByCode(code);
		this.code = code;
		this.message = respuesta.getMessage();
	}

	/**
	 * Constructor que crea una instancia del response a partir de las tres
	 * variables
	 * 
	 * @param code     - codigo de respuesta estandar
	 * @param message  - mensaje de la respuesta
	 * @param response - objeto
	 */
	public GeneralResponse(int code, String message, T response) {
		this.code = code;
		this.message = message;
		this.response = response;
	}

	public T getResponse() {
		return response;
	}

	public void setResponse(T response) {
		this.response = response;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}
