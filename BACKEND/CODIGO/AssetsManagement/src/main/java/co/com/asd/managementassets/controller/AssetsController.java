package co.com.asd.managementassets.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.asd.managementassets.Exception.AsdException;
import co.com.asd.managementassets.business.AssetsBusiness;
import co.com.asd.managementassets.entities.Assets;
import co.com.asd.managementassets.request.EditAssetRequest;
import co.com.asd.managementassets.request.FilterSearchRequest;
import co.com.asd.managementassets.response.GeneralResponse;
import co.com.asd.managementassets.utils.ConstantsUtil.StatusCode;

/**
 * Controlador que expone los metodos necesarios para gestionar los activos
 * 
 * @author dario.buitrago
 *
 */
@RequestMapping("asdgroup/assets")
@RestController
public class AssetsController {

	/**
	 * variable que inyecta las funcionalidades del negocio de empleados
	 */
	@Autowired
	private AssetsBusiness assetsBusiness;

	/**
	 * Método que expone como servicio rest la consulta de todos los activos
	 * 
	 * @param request - request por defecto
	 * @return - Response general que contiene codigo, mensaje y objeto solicitado
	 */
	@CrossOrigin
	@GetMapping(path = "/all")
	public GeneralResponse<List<Assets>> get(HttpServletRequest request) {
		GeneralResponse<List<Assets>> response;
		try {
			response = new GeneralResponse<List<Assets>>(StatusCode.SUCCESS_STATUS, assetsBusiness.getAllAssets());
		} catch (AsdException e) {
			response = new GeneralResponse<>(e.getCode(), e.getMessage() + " - " + e.getSpecificMessage());
		}
		return response;
	}

	/**
	 * Método que expone como servicio rest la creacion de activos
	 * 
	 * @param assetRequest - objeto Asset con sus atributos correspondientes
	 * @param request      - request por defecto
	 * @return - Response general que contiene codigo, mensaje y objeto solicitado
	 */
	@CrossOrigin
	@PutMapping(path = "/create")
	public GeneralResponse<Assets> put(@RequestBody Assets assetRequest, HttpServletRequest request) {
		GeneralResponse<Assets> response;
		try {
			response = new GeneralResponse<Assets>(StatusCode.SUCCESS_STATUS, assetsBusiness.insertAsset(assetRequest));
		} catch (AsdException e) {
			response = new GeneralResponse<>(e.getCode(), e.getMessage() + " - " + e.getSpecificMessage());
		}
		return response;
	}

	/**
	 * Método que expone como servicio rest la modificacion de activos
	 * 
	 * @param editRequest - objeto con el id del activo a modificar y el objeto
	 *                    activo con los nuevos valores
	 * @param request     - request or defecto
	 * @return - Response general que contiene codigo, mensaje y objeto solicitado
	 */
	@CrossOrigin
	@PostMapping(path = "/edit")
	public GeneralResponse<Assets> put(@RequestBody EditAssetRequest editRequest, HttpServletRequest request) {
		GeneralResponse<Assets> response;
		try {
			response = new GeneralResponse<Assets>(StatusCode.SUCCESS_STATUS, assetsBusiness.editAsset(editRequest));
		} catch (AsdException e) {
			response = new GeneralResponse<>(e.getCode(), e.getMessage() + " - " + e.getSpecificMessage());
		}
		return response;
	}

	/**
	 * Método que expone como servicio rest la consulta de activos por cualquiera de
	 * los siguientes filtros (serial, tipo de activo y fecha de compra)
	 * 
	 * @param filterSearchRequest - objeto con los filtros a consultar
	 * @param request             - request por defecto
	 * @return - Response general que contiene codigo, mensaje y objeto solicitado
	 */
	@CrossOrigin
	@PostMapping(path = "/get-by-filter")
	public GeneralResponse<List<Assets>> getByType(@RequestBody FilterSearchRequest filterSearchRequest,
			HttpServletRequest request) {
		GeneralResponse<List<Assets>> response;
		try {
			response = new GeneralResponse<List<Assets>>(StatusCode.SUCCESS_STATUS,
					assetsBusiness.findByFilter(filterSearchRequest));
		} catch (AsdException e) {
			response = new GeneralResponse<>(e.getCode(), e.getMessage() + " - " + e.getSpecificMessage());
		}
		return response;
	}
}
