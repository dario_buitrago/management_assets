package co.com.asd.managementassets.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.asd.managementassets.Exception.AsdException;
import co.com.asd.managementassets.business.EmployeeBusiness;
import co.com.asd.managementassets.entities.Employee;
import co.com.asd.managementassets.response.GeneralResponse;
import co.com.asd.managementassets.utils.ConstantsUtil.StatusCode;

/**
 * Controlador que expone los metodos necesarios para gestionar los empleados
 * 
 * @author dario.buitrago
 *
 */
@RequestMapping("asdgroup/employee")
@RestController
public class EmployeeController {

	/**
	 * variable que inyecta las funcionalidades del negocio de empleados
	 */
	@Autowired
	private EmployeeBusiness employeeBusiness;

	/**
	 * Método que expone como servicio rest la consulta de todos los empleados
	 * 
	 * @param request - request por defecto
	 * @return - Response general que contiene codigo, mensaje y objeto solicitado
	 */
	@CrossOrigin
	@GetMapping(path = "/all")
	public GeneralResponse<List<Employee>> get(HttpServletRequest request) {
		GeneralResponse<List<Employee>> response;
		try {
			response = new GeneralResponse<List<Employee>>(StatusCode.SUCCESS_STATUS,
					employeeBusiness.getAllEmployees());
		} catch (AsdException e) {
			response = new GeneralResponse<>(e.getCode(), e.getMessage() + " - " + e.getSpecificMessage());
		}
		return response;
	}
}
