package co.com.asd.managementassets.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.asd.managementassets.Exception.AsdException;
import co.com.asd.managementassets.entities.Employee;
import co.com.asd.managementassets.repositories.EmployeeRepository;
import co.com.asd.managementassets.utils.ConstantsUtil.StatusCode;

/**
* Clase que contiene la lógica de negocio conserniente a la adminstración de
* Empleados
* 
* @author dario.buitrago
*
*/
@Service
public class EmployeeBusiness {

	/**
	 * variable que inyecta las funcionalidades del repositorio de empleados
	 */
	@Autowired
	private EmployeeRepository employeeRepository;
	
	/**
	 *  Método que invoca la consulta a la base de datos de todos los empleados
	 * @return - Lista de Empleados existentes en la base de datos
	 * @throws AsdException - Excepción de negocio
	 */
	public List<Employee> getAllEmployees() throws AsdException {
		List<Employee> listResult = new ArrayList<>();
		try {
			employeeRepository.findAll().forEach(listResult::add);
			if (listResult.isEmpty()) {
				throw new AsdException(StatusCode.NO_RESULT_STATUS, "No existen Empleados aún");
			}
		} catch (AsdException e) {
			throw e;
		} catch (Exception e) {
			throw new AsdException(StatusCode.INTERNAL_ERROR_STATUS,
					"Se presentó un error mientras se consultaban los empleados");
		}
		return listResult;
	}
}
