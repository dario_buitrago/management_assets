/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.asd.managementassets.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entidad que representa un activo
 * 
 * @author dario.buitrago
 */
@Entity
@Table(name = "assets")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Assets.findAll", query = "SELECT a FROM Assets a"),
		@NamedQuery(name = "Assets.findByAssetId", query = "SELECT a FROM Assets a WHERE a.assetId = :assetId"),
		@NamedQuery(name = "Assets.findByName", query = "SELECT a FROM Assets a WHERE a.name = :name"),
		@NamedQuery(name = "Assets.findByDescription", query = "SELECT a FROM Assets a WHERE a.description = :description"),
		@NamedQuery(name = "Assets.findBySerial", query = "SELECT a FROM Assets a WHERE a.serial = :serial"),
		@NamedQuery(name = "Assets.findByInventoryNumber", query = "SELECT a FROM Assets a WHERE a.inventoryNumber = :inventoryNumber"),
		@NamedQuery(name = "Assets.findByWeight", query = "SELECT a FROM Assets a WHERE a.weight = :weight"),
		@NamedQuery(name = "Assets.findByHight", query = "SELECT a FROM Assets a WHERE a.hight = :hight"),
		@NamedQuery(name = "Assets.findByWidth", query = "SELECT a FROM Assets a WHERE a.width = :width"),
		@NamedQuery(name = "Assets.findByAssetLength", query = "SELECT a FROM Assets a WHERE a.assetLength = :assetLength"),
		@NamedQuery(name = "Assets.findByPurchaseDate", query = "SELECT a FROM Assets a WHERE a.purchaseDate = :purchaseDate"),
		@NamedQuery(name = "Assets.findByPurchasePrice", query = "SELECT a FROM Assets a WHERE a.purchasePrice = :purchasePrice"),
		@NamedQuery(name = "Assets.findByDowngradeDate", query = "SELECT a FROM Assets a WHERE a.downgradeDate = :downgradeDate"),
		@NamedQuery(name = "Assets.findByColor", query = "SELECT a FROM Assets a WHERE a.color = :color") })
public class Assets implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ASSET_ID")
	private Integer assetId;

	@Size(max = 45)
	@Column(name = "NAME")
	private String name;

	@Size(max = 45)
	@Column(name = "DESCRIPTION")
	private String description;

	@Size(max = 45)
	@Column(name = "SERIAL")
	private String serial;

	@Size(max = 45)
	@Column(name = "INVENTORY_NUMBER")
	private String inventoryNumber;

	@Column(name = "WEIGHT")
	private Double weight;

	@Column(name = "HIGHT")
	private Double hight;

	@Column(name = "WIDTH")
	private Double width;

	@Column(name = "ASSET_LENGTH")
	private Double assetLength;

	@Column(name = "PURCHASE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date purchaseDate;

	@Column(name = "PURCHASE_PRICE")
	private Double purchasePrice;

	@Column(name = "DOWNGRADE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date downgradeDate;

	@Size(max = 45)
	@Column(name = "COLOR")
	private String color;

	@JoinColumn(name = "EMPLOYEE_ASIGNED", referencedColumnName = "EMPLOYEE_ID")
	@ManyToOne
	private Employee employeeAsigned;

	@JoinColumn(name = "CURRENT_STATE", referencedColumnName = "STATE_ID")
	@ManyToOne
	private AssetsState currentState;

	@JoinColumn(name = "TYPE", referencedColumnName = "TYPE_ID")
	@ManyToOne
	private AssetsType type;

	public Assets() {
	}

	public Assets(Integer assetId) {
		this.assetId = assetId;
	}

	public Integer getAssetId() {
		return assetId;
	}

	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getInventoryNumber() {
		return inventoryNumber;
	}

	public void setInventoryNumber(String inventoryNumber) {
		this.inventoryNumber = inventoryNumber;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public Date getDowngradeDate() {
		return downgradeDate;
	}

	public void setDowngradeDate(Date downgradeDate) {
		this.downgradeDate = downgradeDate;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Employee getEmployeeAsigned() {
		return employeeAsigned;
	}

	public void setEmployeeAsigned(Employee employeeAsigned) {
		this.employeeAsigned = employeeAsigned;
	}

	public AssetsState getCurrentState() {
		return currentState;
	}

	public void setCurrentState(AssetsState currentState) {
		this.currentState = currentState;
	}

	public AssetsType getType() {
		return type;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getHight() {
		return hight;
	}

	public void setHight(Double hight) {
		this.hight = hight;
	}

	public Double getWidth() {
		return width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Double getAssetLength() {
		return assetLength;
	}

	public void setAssetLength(Double assetLength) {
		this.assetLength = assetLength;
	}

	public Double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(Double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public void setType(AssetsType type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (assetId != null ? assetId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Assets)) {
			return false;
		}
		Assets other = (Assets) object;
		if ((this.assetId == null && other.assetId != null)
				|| (this.assetId != null && !this.assetId.equals(other.assetId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.asd.managementassets.entities.Assets[ assetId=" + assetId + " ]";
	}

}
