package co.com.asd.managementassets.Exception;

import co.com.asd.managementassets.utils.CodigoRespuestaEnum;

/**
 * Excepcion personalizada para estandarizar las anomalias de negocio
 * 
 * @author dario.buitrago
 *
 */
public class AsdException extends Exception {

	private static final long serialVersionUID = 1L;
	private String mensaje;
	private int code;
	private String specificMessage;

	/**
	 * Constructor que crea una instancia de la excepcion a partir del codigo de
	 * error
	 * 
	 * @param code - codigo estandar de error
	 */
	public AsdException(int code) {
		super();
		CodigoRespuestaEnum respuesta = CodigoRespuestaEnum.findByCode(code);
		this.mensaje = respuesta.getMessage();
		this.code = respuesta.getCode();
	}

	/**
	 * Constructor que crea una instancia de la excepcion a partir del codigo de
	 * error y del mensaje especifico
	 * 
	 * @param code - codigo estandar de error
	 * @param specificMessage - mensaje especifico necesario cuando la excepcion es por causa desconocida
	 */
	public AsdException(int code, String specificMessage) {
		super();
		CodigoRespuestaEnum respuesta = CodigoRespuestaEnum.findByCode(code);
		this.mensaje = respuesta.getMessage();
		this.code = respuesta.getCode();
		this.specificMessage = specificMessage;
	}

	public String getMessage() {
		return mensaje;
	}

	public int getCode() {
		return code;
	}

	public String getSpecificMessage() {
		return specificMessage;
	}

}
