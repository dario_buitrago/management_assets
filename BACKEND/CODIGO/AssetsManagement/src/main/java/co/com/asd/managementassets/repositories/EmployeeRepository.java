package co.com.asd.managementassets.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.com.asd.managementassets.entities.Employee;

/**
 * interface que contiene los metodos de consulta a la tabla de los empleados
 * 
 * @author dario.buitrago
 *
 */
@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

	/**
	 * Consulta a la base de datos que devuelve el empleado identificado con el id
	 * que pasa como parametro
	 * 
	 * @param employeeId - id del empleado
	 * @return - empleado identificado con el id que pasa como parametro
	 */
	public Employee findByEmployeeId(Integer employeeId);
}
