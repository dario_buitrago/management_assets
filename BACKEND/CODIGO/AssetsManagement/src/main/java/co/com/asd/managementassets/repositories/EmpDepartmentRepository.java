package co.com.asd.managementassets.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.com.asd.managementassets.entities.EmpDepartment;

/**
 * interface que contiene los metodos de consulta a la tabla de las áreas
 * 
 * @author dario.buitrago
 *
 */
@Repository
public interface EmpDepartmentRepository extends CrudRepository<EmpDepartment, Integer> {

}
