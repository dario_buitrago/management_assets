package co.com.asd.managementassets.request;

import java.io.Serializable;
import java.util.Date;

/**
 * Request utilizado para pasar los parametros necesarios en la busqueda de
 * activos filtrados
 * 
 * @author dario.buitrago
 *
 */
public class FilterSearchRequest implements Serializable {

	private static final long serialVersionUID = -7588157184266891380L;

	private Integer typeId;
	private String serial;
	private Date startDate;
	private Date endDate;

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
