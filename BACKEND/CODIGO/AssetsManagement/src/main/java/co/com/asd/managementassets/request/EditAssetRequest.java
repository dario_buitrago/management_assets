package co.com.asd.managementassets.request;

import java.io.Serializable;

import co.com.asd.managementassets.entities.Assets;

/**
 * Request utilizado para pasar los parametros de modificacion de activos
 * 
 * @author dario.buitrago
 *
 */
public class EditAssetRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer assetIdToEdit;
	private Assets newAsset;

	public Integer getAssetIdToEdit() {
		return assetIdToEdit;
	}

	public void setAssetIdToEdit(Integer assetIdToEdit) {
		this.assetIdToEdit = assetIdToEdit;
	}

	public Assets getNewAsset() {
		return newAsset;
	}

	public void setNewAsset(Assets newAsset) {
		this.newAsset = newAsset;
	}

}
