/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.asd.managementassets.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entidad que representa un departamento del pais
 * 
 * @author dario.buitrago
 */
@Entity
@Table(name = "location_department")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "LocationDepartment.findAll", query = "SELECT l FROM LocationDepartment l"),
		@NamedQuery(name = "LocationDepartment.findByLocDepartmentId", query = "SELECT l FROM LocationDepartment l WHERE l.locDepartmentId = :locDepartmentId"),
		@NamedQuery(name = "LocationDepartment.findByName", query = "SELECT l FROM LocationDepartment l WHERE l.name = :name") })
public class LocationDepartment implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "LOC_DEPARTMENT_ID")
	private Integer locDepartmentId;

	@Size(max = 45)
	@Column(name = "NAME")
	private String name;

	@OneToMany(mappedBy = "departmentId")
	@JsonIgnore
	private Collection<City> cityCollection;

	public LocationDepartment() {
	}

	public LocationDepartment(Integer locDepartmentId) {
		this.locDepartmentId = locDepartmentId;
	}

	public Integer getLocDepartmentId() {
		return locDepartmentId;
	}

	public void setLocDepartmentId(Integer locDepartmentId) {
		this.locDepartmentId = locDepartmentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlTransient
	public Collection<City> getCityCollection() {
		return cityCollection;
	}

	public void setCityCollection(Collection<City> cityCollection) {
		this.cityCollection = cityCollection;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (locDepartmentId != null ? locDepartmentId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof LocationDepartment)) {
			return false;
		}
		LocationDepartment other = (LocationDepartment) object;
		if ((this.locDepartmentId == null && other.locDepartmentId != null)
				|| (this.locDepartmentId != null && !this.locDepartmentId.equals(other.locDepartmentId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.asd.managementassets.entities.LocationDepartment[ locDepartmentId=" + locDepartmentId + " ]";
	}

}
