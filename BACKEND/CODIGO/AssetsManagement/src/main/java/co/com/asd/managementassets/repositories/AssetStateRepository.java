package co.com.asd.managementassets.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.com.asd.managementassets.entities.AssetsState;

/**
 * interface que contiene los metodos de consulta a la tabla de los estados de
 * activos
 * 
 * @author dario.buitrago
 *
 */
@Repository
public interface AssetStateRepository extends CrudRepository<AssetsState, Integer> {

	/**
	 * Consulta a la base de datos que devuelve el estado identificado con el id que
	 * pasa como parametro
	 * 
	 * @param stateId - id del estado del activo
	 * @return - estado identificado con el id que pasa como parametro
	 */
	public AssetsState findByStateId(Integer stateId);
}
