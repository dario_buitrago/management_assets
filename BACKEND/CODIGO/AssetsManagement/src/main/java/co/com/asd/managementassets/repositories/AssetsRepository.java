package co.com.asd.managementassets.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.com.asd.managementassets.entities.Assets;
import co.com.asd.managementassets.entities.AssetsType;

/**
 * interface que contiene los metodos de consulta a la tabla de los activos
 * 
 * @author dario.buitrago
 *
 */
@Repository
public interface AssetsRepository extends CrudRepository<Assets, Integer> {

	/**
	 * Consulta a la base de datos que devuelve un activo por el id que pasa como
	 * parametro
	 * 
	 * @param assetId - id del activo que se busca
	 * @return - activo de la base de datos si existe o null si no
	 */
	public Assets findByAssetId(Integer assetId);

	/**
	 * Consulta a la base de datos que devuelve todos los activos que coinciden con
	 * el tipo de activo que pasa como parametro
	 * 
	 * @param assetsType - tipo de parametro
	 * @return - lista de activos que coinciden con el tipo buscado
	 */
	public List<Assets> findByType(AssetsType assetsType);

	/**
	 * Consulta a la base de datos que devuelve todos los activos que coinciden con
	 * el serial que pasa como parametro
	 * 
	 * @param serial - serial del activo
	 * @return - lista de los activos que coinciden con el serial que paso como
	 *         parametro
	 */
	public List<Assets> findBySerial(String serial);

	/**
	 * Consulta a la base de datos que devuelve todos los activos fueron comprados
	 * en un rango de fechas que pasa como parametro
	 * 
	 * @param start - limite inferior del rango de fechas a buscar
	 * @param end   - limite superior del rango de fechas a buscar
	 * @return - lista de activos comprados en las fechas que se pasaron como
	 *         parametros
	 */
	public List<Assets> findByPurchaseDateGreaterThanAndPurchaseDateLessThanEqual(Date start, Date end);

	/**
	 * Consulta a la base de datos que devuelve la lista de activos con el serial y
	 * tipo de activo que pasan como parametro
	 * 
	 * @param serial     - serial del activo
	 * @param assetsType - tipo del activo
	 * @return - lista de activos con el serial y tipo de activo que pasan como
	 *         parametro
	 */
	public List<Assets> findBySerialAndType(String serial, AssetsType assetsType);

	/**
	 * Consulta a la base de datos que devuelve la lista de activos con el serial y
	 * comprado en un rango de fechas que pasan como parametro
	 * 
	 * @param start  - limite inferior del rango de fechas a buscar
	 * @param end    - limite superior del rango de fechas a buscar
	 * @param serial - serial del activo
	 * @return - lista de activos con el serial y comprado en un rango de fechas que
	 *         pasan como parametro
	 */
	public List<Assets> findByPurchaseDateGreaterThanAndPurchaseDateLessThanEqualAndSerial(Date start, Date end,
			String serial);

	/**
	 * Consulta a la base de datos que devuelve la lista de activos con el tipo de
	 * activo y comprado en un rango de fechas que pasan como parametro
	 *
	 * @param start      - limite inferior del rango de fechas a buscar
	 * @param end        - limite superior del rango de fechas a buscar
	 * @param assetsType - tipo del activo
	 * @return - lista de activos con el tipo de activo y comprado en un rango de
	 *         fechas que pasan como parametro
	 */
	public List<Assets> findByPurchaseDateGreaterThanAndPurchaseDateLessThanEqualAndAndType(Date start, Date end,
			AssetsType assetsType);

	/**
	 * Consulta a la base de datos que devuelve la lista de activos con el serial,
	 * tipo de activo y comprado en un rango de fechas que pasan como parametro
	 *
	 * @param start      - limite inferior del rango de fechas a buscar
	 * @param end        - limite superior del rango de fechas a buscar
	 * @param assetsType - tipo del activo
	 * @param serial     - serial del activo
	 * @return - lista de activos con el serial, tipo de activo y comprado en un
	 *         rango de fechas que pasan como parametro
	 */
	public List<Assets> findByPurchaseDateGreaterThanAndPurchaseDateLessThanEqualAndSerialAndType(Date start, Date end,
			String serial, AssetsType assetsType);

}
