/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.asd.managementassets.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entidad que representa el estado de un activo
 * 
 * @author dario.buitrago
 */
@Entity
@Table(name = "assets_state")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "AssetsState.findAll", query = "SELECT a FROM AssetsState a"),
		@NamedQuery(name = "AssetsState.findByStateId", query = "SELECT a FROM AssetsState a WHERE a.stateId = :stateId"),
		@NamedQuery(name = "AssetsState.findByName", query = "SELECT a FROM AssetsState a WHERE a.name = :name") })
public class AssetsState implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "STATE_ID")
	private Integer stateId;

	@Size(max = 45)
	@Column(name = "NAME")
	private String name;

	@JsonIgnore
	@OneToMany(mappedBy = "currentState")
	private Collection<Assets> assetsCollection;

	public AssetsState() {
	}

	public AssetsState(Integer stateId) {
		this.stateId = stateId;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlTransient
	public Collection<Assets> getAssetsCollection() {
		return assetsCollection;
	}

	public void setAssetsCollection(Collection<Assets> assetsCollection) {
		this.assetsCollection = assetsCollection;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (stateId != null ? stateId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof AssetsState)) {
			return false;
		}
		AssetsState other = (AssetsState) object;
		if ((this.stateId == null && other.stateId != null)
				|| (this.stateId != null && !this.stateId.equals(other.stateId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.asd.managementassets.entities.AssetsState[ stateId=" + stateId + " ]";
	}

}
