package co.com.asd.managementassets.utils;

/**
 * Clase que almacena las constantes usadas en el sistema
 * 
 * @author dario.buitrago
 *
 */
public class ConstantsUtil {

	/**
	 * sub-clase que contiene las constantes correspondientes a los codigos de
	 * respuesta de los servicios
	 * 
	 * @author dario.buitrago
	 *
	 */
	public static class StatusCode {
		public static final int SUCCESS_STATUS = 200;
		public static final int BAD_REQUEST_STATUS = 400;
		public static final int NO_RESULT_STATUS = 404;
		public static final int INTERNAL_ERROR_STATUS = 500;
	}
}
