package co.com.asd.managementassets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Clase que proporciona las caracteristicas de aplicacion al proyecto
 * @author dario.buitrago
 *
 */
@SpringBootApplication
public class AssetsManagementApplication {

	/**
	 * Método principal del lanzador de la aplicación
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(AssetsManagementApplication.class, args);
	}
}
