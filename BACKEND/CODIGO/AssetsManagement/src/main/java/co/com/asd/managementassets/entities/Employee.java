/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.asd.managementassets.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entidad que representa un empleado
 * 
 * @author dario.buitrago
 */
@Entity
@Table(name = "employee")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e"),
		@NamedQuery(name = "Employee.findByEmployeeId", query = "SELECT e FROM Employee e WHERE e.employeeId = :employeeId"),
		@NamedQuery(name = "Employee.findByDocument", query = "SELECT e FROM Employee e WHERE e.document = :document"),
		@NamedQuery(name = "Employee.findByDocumentType", query = "SELECT e FROM Employee e WHERE e.documentType = :documentType"),
		@NamedQuery(name = "Employee.findByPosition", query = "SELECT e FROM Employee e WHERE e.position = :position"),
		@NamedQuery(name = "Employee.findByName", query = "SELECT e FROM Employee e WHERE e.name = :name"),
		@NamedQuery(name = "Employee.findByLastName", query = "SELECT e FROM Employee e WHERE e.lastName = :lastName") })
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "EMPLOYEE_ID")
	private Integer employeeId;

	@Size(max = 45)
	@Column(name = "DOCUMENT")
	private String document;

	@Size(max = 45)
	@Column(name = "DOCUMENT_TYPE")
	private String documentType;

	@Size(max = 45)
	@Column(name = "POSITION")
	private String position;

	@Size(max = 45)
	@Column(name = "NAME")
	private String name;

	@Size(max = 45)
	@Column(name = "LAST_NAME")
	private String lastName;

	@OneToMany(mappedBy = "employeeAsigned")
	@JsonIgnore
	private Collection<Assets> assetsCollection;

	@JoinColumn(name = "EMP_DEPARTMENT_ID", referencedColumnName = "EMP_DEPARTMENT_ID")
	@ManyToOne
	private EmpDepartment empDepartmentId;

	public Employee() {
	}

	public Employee(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@XmlTransient
	public Collection<Assets> getAssetsCollection() {
		return assetsCollection;
	}

	public void setAssetsCollection(Collection<Assets> assetsCollection) {
		this.assetsCollection = assetsCollection;
	}

	public EmpDepartment getEmpDepartmentId() {
		return empDepartmentId;
	}

	public void setEmpDepartmentId(EmpDepartment empDepartmentId) {
		this.empDepartmentId = empDepartmentId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (employeeId != null ? employeeId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Employee)) {
			return false;
		}
		Employee other = (Employee) object;
		if ((this.employeeId == null && other.employeeId != null)
				|| (this.employeeId != null && !this.employeeId.equals(other.employeeId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.asd.managementassets.entities.Employee[ employeeId=" + employeeId + " ]";
	}

}
