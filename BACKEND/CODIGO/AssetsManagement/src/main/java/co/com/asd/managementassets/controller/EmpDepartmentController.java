package co.com.asd.managementassets.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.asd.managementassets.Exception.AsdException;
import co.com.asd.managementassets.business.DepartmentBusiness;
import co.com.asd.managementassets.entities.EmpDepartment;
import co.com.asd.managementassets.response.GeneralResponse;
import co.com.asd.managementassets.utils.ConstantsUtil.StatusCode;

/**
 * Controlador que expone los metodos necesarios para gestionar las áreas de trabajo
 * 
 * @author dario.buitrago
 *
 */
@RequestMapping("asdgroup/departments")
@RestController
public class EmpDepartmentController {

	/**
	 * variable que inyecta las funcionalidades del negocio de departamentos
	 */
	@Autowired
	private DepartmentBusiness departmentBusiness;

	/**
	 * Método que expone como servicio rest la consulta de todas las áreas
	 * 
	 * @param request - request por defecto
	 * @return - Response general que contiene codigo, mensaje y objeto solicitado
	 */
	@CrossOrigin
	@GetMapping(path = "/all")
	public GeneralResponse<List<EmpDepartment>> get(HttpServletRequest request) {
		GeneralResponse<List<EmpDepartment>> response;
		try {
			response = new GeneralResponse<List<EmpDepartment>>(StatusCode.SUCCESS_STATUS,
					departmentBusiness.getAllEmpDepartments());
		} catch (AsdException e) {
			response = new GeneralResponse<>(e.getCode(), e.getMessage() + " - " + e.getSpecificMessage());
		}
		return response;
	}
}
