package co.com.asd.managementassets.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.com.asd.managementassets.entities.AssetsType;

/**
 * interface que contiene los metodos de consulta a la tabla de los tipo de
 * activo
 * 
 * @author dario.buitrago
 *
 */
@Repository
public interface AssetTypeRepository extends CrudRepository<AssetsType, Integer> {

	/**
	 * Consulta a la base de datos que devuelve el tipo de activo identificado con
	 * el id que pasa como parametro
	 * 
	 * @param stateId - id del tipo del activo
	 * @return - tipo de activo identificado con el id que pasa como parametro
	 */
	public AssetsType findByTypeId(Integer typeId);
}
