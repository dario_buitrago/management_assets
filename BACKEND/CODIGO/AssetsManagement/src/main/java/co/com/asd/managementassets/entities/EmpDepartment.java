/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.asd.managementassets.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entidad que representa una área de trabajo o departamento
 * 
 * @author dario.buitrago
 */
@Entity
@Table(name = "emp_department")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "EmpDepartment.findAll", query = "SELECT e FROM EmpDepartment e"),
		@NamedQuery(name = "EmpDepartment.findByEmpDepartmentId", query = "SELECT e FROM EmpDepartment e WHERE e.empDepartmentId = :empDepartmentId"),
		@NamedQuery(name = "EmpDepartment.findByName", query = "SELECT e FROM EmpDepartment e WHERE e.name = :name") })
public class EmpDepartment implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "EMP_DEPARTMENT_ID")
	private Integer empDepartmentId;

	@Size(max = 45)
	@Column(name = "NAME")
	private String name;

	@JoinColumn(name = "CITY", referencedColumnName = "CITY_ID")
	@ManyToOne
	private City city;

	@JsonIgnore
	@OneToMany(mappedBy = "empDepartmentId")
	private Collection<Employee> employeeCollection;

	public EmpDepartment() {
	}

	public EmpDepartment(Integer empDepartmentId) {
		this.empDepartmentId = empDepartmentId;
	}

	public Integer getEmpDepartmentId() {
		return empDepartmentId;
	}

	public void setEmpDepartmentId(Integer empDepartmentId) {
		this.empDepartmentId = empDepartmentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@XmlTransient
	public Collection<Employee> getEmployeeCollection() {
		return employeeCollection;
	}

	public void setEmployeeCollection(Collection<Employee> employeeCollection) {
		this.employeeCollection = employeeCollection;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (empDepartmentId != null ? empDepartmentId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof EmpDepartment)) {
			return false;
		}
		EmpDepartment other = (EmpDepartment) object;
		if ((this.empDepartmentId == null && other.empDepartmentId != null)
				|| (this.empDepartmentId != null && !this.empDepartmentId.equals(other.empDepartmentId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "co.com.asd.managementassets.entities.EmpDepartment[ empDepartmentId=" + empDepartmentId + " ]";
	}

}
