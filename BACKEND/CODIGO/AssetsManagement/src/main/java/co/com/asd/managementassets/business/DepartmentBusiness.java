package co.com.asd.managementassets.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.asd.managementassets.Exception.AsdException;
import co.com.asd.managementassets.entities.EmpDepartment;
import co.com.asd.managementassets.repositories.EmpDepartmentRepository;
import co.com.asd.managementassets.utils.ConstantsUtil.StatusCode;

/**
 * Clase que contiene la lógica de negocio conserniente a la adminstración de
 * Áreas de trabajo
 * 
 * @author dario.buitrago
 *
 */
@Service
public class DepartmentBusiness {

	/**
	 * variable que inyecta las funcionalidades del repositorio de areas de trabajo
	 */
	@Autowired
	private EmpDepartmentRepository empDepartmentRepository;

	/**
	 * Método que invoca la consulta a la base de datos de todos los departamentos
	 * 
	 * @return - Lista de Departamentos existentes en la base de datos
	 * @throws AsdException - Excepción de negocio
	 */
	public List<EmpDepartment> getAllEmpDepartments() throws AsdException {
		List<EmpDepartment> listResult = new ArrayList<>();
		try {
			empDepartmentRepository.findAll().forEach(listResult::add);
			if (listResult.isEmpty()) {
				throw new AsdException(StatusCode.NO_RESULT_STATUS, "No existen áreas de trabájo aún");
			}
		} catch (AsdException e) {
			throw e;
		} catch (Exception e) {
			throw new AsdException(StatusCode.INTERNAL_ERROR_STATUS,
					"Se presentó un error mientras se consultaban las áreas de trabájo");
		}
		return listResult;
	}

}
