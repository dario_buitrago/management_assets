package co.com.asd.managementassets.utils;

import co.com.asd.managementassets.utils.ConstantsUtil.StatusCode;

/**
 * Enum con los mensajes principales y sus codigos para ser llamados al
 * instanciar responses
 * 
 * @author dario.buitrago
 *
 */
public enum CodigoRespuestaEnum {
	RESPONSE_SUCCESS(StatusCode.SUCCESS_STATUS, "Solicitud procesada satisfactoriamente"),
	RESPONSE_BAD_REQUEST(StatusCode.BAD_REQUEST_STATUS, "Datos faltantes para procesar la solicitud"),
	RESPONSE_NO_RESULT(StatusCode.NO_RESULT_STATUS, "La solicitud no contiene resultados"),
	RESPONSE_INTERNAL_ERROR(StatusCode.INTERNAL_ERROR_STATUS, "Ocurrio un error mientras se procesaba la solicitud");

	private int code;
	private String message;

	/**
	 * Codigo constructor de la enumeracion
	 * 
	 * @param code    {@link StatusCode} que me indica el codigo de respuesta a ser
	 *                controlado en la enumeracion
	 * @param message Mensaje de respuesta a ser auditado
	 */
	private CodigoRespuestaEnum(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	/**
	 * Método que extrae el mensaje parametrizado a partir del codigo estandar
	 * 
	 * @param code - codigo estandar
	 * @return - mensaje correspondiente al codigo
	 */
	public static CodigoRespuestaEnum findByCode(int code) {
		for (CodigoRespuestaEnum tipo : CodigoRespuestaEnum.values()) {
			if (tipo.code == code) {
				return tipo;
			}
		}
		throw new IllegalArgumentException("Mensaje de respuesta no parametrizado : " + code);
	}
}
